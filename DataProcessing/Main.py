from DataExtractor.PandasDataExtraction import PandasDataExtractor
from DataProcessor.PandasDataProcessing import PandasDataProcessor
from DataProcessor.TextFileProcessing import TextFileProcessor
from Plotting.BaseInterface import *
from Plotting.BarChart import *
from Plotting.Scatter import *

text_processor = TextFileProcessor()

data_processor = PandasDataProcessor()
#data_processor.pandas_read_excel()

scatter = ScatterPlot()
scatter.plot_scatter_data()
