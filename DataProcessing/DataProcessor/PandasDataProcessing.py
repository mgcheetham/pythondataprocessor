import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

class PandasDataProcessor:

    #data array for each column value
    element_oxides_index = ['Na2O','MgO','Al2O3','SiO2','P2O5','SO3',
                            'K2O','CaO','TiO2','Cr2O3','MnO','FeO']

    def pandas_read_excel(self):

        data = pd.read_excel('\\martian_rocks.xlsx',sheet_name='Sheet1')

        row_types = ["RU", "RB", "RR", "RF", "RR1", "RR2"]
        
        for type in row_types:
            self.pandas_create_bar_diagram(data, type)

    
    def pandas_create_bar_diagram(self, data_set, type):

        data_objects = {}

        for index, row in data_set.iterrows():
            #print(row)
            if row['Type'] == type:
                #print(row)
                data_objects[row['Sample']] = [row[5], row[7], row[9], row[11], 
                                                row[13], row[15], row[19], row[21], 
                                                row[23], row[25], row[27], row[29]]

        df = pd.DataFrame(data_objects, index=self.element_oxides_index)
        

        matplot_axis = df.plot.bar(rot=0)
        matplot_axis.set_title(F"{type} Sample Type(s)")
        matplot_axis.set_xlabel("Element-Oxides")
        matplot_axis.set_ylabel("Sample Type Value")
        #matplot_axis.yaxis.grid(True)

        #plt.savefig(f'{type}_samples.jpeg', dpi=300, bbox_inches='tight')
        #figure = plt.figure(dpi=200)
        plt.show()

    def pandas_excel_file(self):

        xlsx = pd.ExcelFile('\\martian_rocks.xlsx')

        data = xlsx.parse(0)

        print(data)



