class TextFileProcessor:

    def begin_process():
        """ main entry function """

        #load both datasets
        agil_data = process_datasets("MS_AGILKIA_1.txt")
        aby_data = process_datasets("MS_ABYDOS_1.txt")
        #call plotting function with parsed data
        plotting_data(agil_data[0], agil_data[1], aby_data[0], aby_data[1])

    def process_datasets(data_set):
      
        """ function to read and process a file """

        #holding arrays for parsed data
        molecular_mass = []
        intensity_value = []
  
        #Open/Read file
        file = open(data_set, "r")
  
        #loop over each line in file
        for line in file:
    
            #split each line on whitespace
            #to get each individual value
            split_line = line.split()
    
            #print(split_line)
    
            #add int value to array
            molecular_mass.append(int(split_line[0]))

            #convert string values to int
            int_first_scan = int(split_line[1])
            int_second_scan = int(split_line[2])
    
            #checks for zeroed values
            if int_first_scan == 0 or int_second_scan == 0:
      
                zero_sum_value = int_first_scan + int_second_scan
      
                #call scaling factor based on specific dataset
                if data_set == "MS_AGILKIA_1.txt":
                    zero_sum_value = scaling_factor("AGI", zero_sum_value)
                elif data_set == "MS_ABYDOS_1.txt":
                    zero_sum_value = scaling_factor("ABY", zero_sum_value)
                #add scaled value to array
                intensity_value.append(zero_sum_value)
            else:
                intensity_value.append(int_first_scan + int_second_scan)
  
        #returns both arrays
        return molecular_mass, intensity_value

    def plotting_data(agil_molecular_mass, agil_intensity_value, aby_molecular_mass, aby_intensity_value):
        
        """ function handles plotting data """

        #plot each dataset using matplotlib
        plt.plot(aby_molecular_mass, aby_intensity_value, 'ro')
        plt.plot(agil_molecular_mass, agil_intensity_value, 'bo')
        #add labels
        plt.ylabel('Intensity value')
        plt.xlabel('Molecular mass')
        #print the graph
        plt.show()

    def scaling_factor(dataset_name, original_value):
        
        """ function handles applying scaling factor """
        
        #return new scaled value
        #based on dataset scaling factor
        if dataset_name == "AGI":
            return original_value * 1.35
        elif dataset_name == "ABY":
            return original_value * 1.47
