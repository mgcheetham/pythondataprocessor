from Plotting.BaseInterface import *
import matplotlib.pyplot as plt
import pandas as pd

class ScatterPlot(BaseInterface):
    
    def __init__(self, x_axis, y_axis, ranges):
        self.x_axis_data = x_axis
        self.y_axis_data = y_axis
        self.ranges = ranges

    def plot_scatter_data(self):

        #plot each dataset using matplotlib
        plt.scatter(self.x_axis_data, self.y_axis_data, s=self.ranges)

        #title
        plt.title("Scatter diagram")

        #add labels
        plt.ylabel('Intensity value')
        plt.xlabel('Molecular mass')

        #print the graph
        plt.show()