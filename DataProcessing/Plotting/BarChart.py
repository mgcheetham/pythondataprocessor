from Plotting.BaseInterface import *
import matplotlib.pyplot as plt
import pandas as pd
from Plotting.Colour import * 
import numpy as np

class BarChart(BaseInterface):

    group_1 = np.array([4,7,1,9])
    group_2 = np.array([3,8,1,10])
    group_3 = np.array([22,13,9,3])
    labels = np.array(["Nb", "Tc", "Na2O", "SEd"])

    def plot_data(self):

        bar_width = 0.25

        r1 = np.arange(len(self.group_1))
        r2 = [x + bar_width for x in r1]
        r3 = [x + bar_width for x in r2]
 
        # Make the plot
        plt.bar(r1, self.group_1, color=ColourGenerator(), width=bar_width, edgecolor='white', label='var1')
        plt.bar(r2, self.group_2, color=ColourGenerator(), width=bar_width, edgecolor='white', label='var2')
        plt.bar(r3, self.group_3, color=ColourGenerator(), width=bar_width, edgecolor='white', label='var3')
 
        # Add xticks on the middle of the group bars
        plt.xlabel('Elements', fontweight='bold')
        plt.xticks([r + bar_width for r in range(len(self.group_1))], self.labels)
 
        # Create legend & Show graphic
        plt.legend()
        plt.show()

