import abc

class BaseInterface(metaclass=abc.ABCMeta):

    """ https://realpython.com/python-interface/ """

    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'plot_data') and 
                callable(subclass.plot_data))

    @abc.abstractmethod
    def plot_data(self):
        raise NotImplemented