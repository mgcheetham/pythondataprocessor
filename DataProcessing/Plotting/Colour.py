import random as rm

class ColourGenerator:

    def __init__(self):

        random_number = rm.randint(0,16777215)
        hex_number = str(hex(random_number))
        hex_number ='#'+ hex_number[2:]

        return hex_number
