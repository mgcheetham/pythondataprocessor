import xlrd as xd
import openpyxl as oxl

class PandasDataExtractor:
    
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)

    def read_document(self):

        file_location = ("\\martian_rocks.xlsx")
 
        file = oxl.load_workbook(file_location, read_only = True)
        sheet = file["Sheet1"]

        for row in sheet.rows:

            if row.index == 1:

                for cell in row:

                    print(cell.value)

        file.close();

